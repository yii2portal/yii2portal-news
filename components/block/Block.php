<?php


namespace yii2portal\news\components\block;

use yii2portal\core\components\Widget;

class Block extends Widget
{

    /**
     * @param $view
     * @return BlockNewsQuery
     */
    public function insert($view)
    {
        $query = new BlockNewsQuery('yii2portal\news\models\News');
        $query->setContext($this);
        $query->setView($view);
        return $query;
    }

    /**
     * @param $items array
     * @param $blockNewsQuery BlockNewsQuery
     */
    public function finish($data, $blockNewsQuery)
    {

        return $this->render($blockNewsQuery->getView(), [
            'data' => $data,
            'newsQuery' => $blockNewsQuery
        ]);
    }

}