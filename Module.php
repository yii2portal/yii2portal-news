<?php

namespace yii2portal\news;

use Yii;
use yii\web\View;
use yii2portal\news\components\cplugin\Simnews;
use yii2portal\news\models\News;

class Module extends \yii2portal\core\Module
{

    public $controllerNamespace = 'yii2portal\news\controllers';

    private $_lastEditedGlobal = null;
    private $_nextDelayedGlobal = -1;

    public function init()
    {
        parent::init();

        Yii::$app->getModule('cplugin')->registerPlugin(Simnews::className());

        if(!Yii::$app->request->isAjax) {
            Yii::$app->view->on(View::EVENT_END_BODY, function () {
                $newsCategories = Yii::$app->getModule('structure')->getPagesByModule('news');
                foreach ($newsCategories as $category) {
                    Yii::$app->view->registerLinkTag([
                        'rel' => 'alternate',
                        'type' => 'application/rss+xml',
                        'title' => $category->title,
                        'href' => "{$category->urlPath}rss/"
                    ]);
                }
            });
        }


    }


    public function getLastEditedGlobal(){
        if($this->_lastEditedGlobal == null){
            $this->_lastEditedGlobal = News::find()
                ->andPublished()
                ->byDatepublic()
                ->max('datelasteuserdit');
        }

        return $this->_lastEditedGlobal;
    }



    public function getNextDelayedGlobal(){
        if($this->_nextDelayedGlobal == -1){
            $this->_nextDelayedGlobal = News::find()->where(['>', 'datepublic', time()])->min('datepublic');
        }
        return $this->_nextDelayedGlobal;
    }


}