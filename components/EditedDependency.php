<?php


namespace yii2portal\news\components;

use Yii;
use yii\caching\Dependency;

class EditedDependency extends Dependency{
    protected function generateDependencyData($cache)
    {
        return Yii::$app->getModule('news')->getLastEditedGlobal();
    }
}