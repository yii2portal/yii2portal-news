<?php

namespace yii2portal\news\components\block;

use yii2portal\news\models\News;
use yii2portal\news\models\NewsQuery;


/**
 * This is the ActiveQuery class for [[News]].
 *
 * @see News
 */
class BlockNewsQuery extends NewsQuery
{

    /**
     * @var Block
     */
    protected $_context;

    /**
     * @var string
     */
    protected $_view;

    /**
     * @var News[]
     */
    private $_fetched;

    public function all($db = null)
    {
        if(!$this->_fetched) {
            $this->_fetched = parent::all($db);
        }
        return $this;
    }

    public function one($db = null)
    {
        if(!$this->_fetched) {
            $this->_fetched = parent::one($db);
        }
        return $this;
    }

    public function fetched(){
        return $this->_fetched;
    }


    public function setContext($context)
    {
        $this->_context = $context;
    }

    public function setView($view)
    {
        $this->_view = $view;
    }

    public function getView()
    {
        return $this->_view;
    }

    public function __toString()
    {

        return $this->_context->finish($this->_fetched, $this);
    }
}
