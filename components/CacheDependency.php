<?php


namespace yii2portal\news\components;

use Yii;
use yii\caching\ChainedDependency;

class CacheDependency extends ChainedDependency
{
    public function init()
    {
        parent::init();
        $this->dependencies[] = Yii::createObject([
            'class' => 'yii2portal\news\components\EditedDependency'
        ]);
        $this->dependencies[] = Yii::createObject([
            'class' => 'yii2portal\news\components\DelayedDependency'
        ]);
    }
}