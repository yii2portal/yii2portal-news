<?php


return [
    [
        'pattern' => "",
        'route' => "index/index",
        'defaults' => [
            'page' => 1
        ]
    ],
    [
        'pattern' => "rss",
        'route' => "rss/index"
    ],
    [
        'pattern' => "page_<page:\d+>",
        'route' => "index/index",
        'defaults' => [
            'page' => 1
        ]
    ],
    [
        'pattern' => "<item_id:\d+>_<translit>/",
        'route' => "index/view",
    ],
    [
        'pattern' => "<item_id:\d+>/",
        'route' => "index/view",
    ]
];