<?php

namespace yii2portal\news\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;


/**
 * This is the ActiveQuery class for [[News]].
 *
 * @see News
 */
class NewsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return News[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return News|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }


    public function byDatepublic($mintime = NULL, $maxtime = NULL)
    {
        if ($mintime == NULL)
            return $this->andFilterCompare('datepublic', new Expression("UNIX_TIMESTAMP()"), "<=");
        elseif ($maxtime == NULL) {
            return $this->andWhere(['between', 'datepublic', $mintime, new Expression("UNIX_TIMESTAMP()")]);
        } else {
            return $this->andWhere(['between', 'datepublic', $mintime, $maxtime]);
        }
    }

    /**
     * @param int $order
     * @return $this
     */
    public function byPopularity($order = SORT_DESC)
    {
        $this->orderBy([
            'cviews'=>$order
        ]);
        return $this;
    }


    /**
     * @param $moduleName
     * @return $this
     */
    public function andModules($modulesName)
    {
        $pages = Yii::$app->getModule('structure')->getPagesByModule($modulesName);
        return $this->andWhere([
            'pid' => ArrayHelper::getColumn($pages, 'id')
        ]);
    }

    /**
     * @param $moduleName
     * @return $this
     */
    public function andNotModules($modulesName)
    {
        $pages = Yii::$app->getModule('structure')->getPagesByModule($modulesName);
        return $this->andWhere([
            'not', 'pid', ArrayHelper::getColumn($pages, 'id')
        ]);
    }


    /**
     * @return $this
     */
    public function andPublished()
    {
        return $this->andWhere([
            'status' => 1
        ]);
    }

    /**
     * @return $this
     */
    public function andMain()
    {
        return $this->andWhere([
            'is_main' => 1
        ]);
    }

    public function withAnyImage(){
        return $this->with('baseImage', 'imageLenta', 'imageMain', 'imageCol');
    }

    /**
     * @return $this
     */
    public function andTags($tag)
    {
        $tags = is_array($tag) ? $tag : array($tag);

        $sql = [];
        foreach ($tags as $tag) {
            $sql[] = "FIND_IN_SET({$tag}, tags)";
        }

        return !empty($sql) ? $this->andWhere(new Expression(implode(" OR ", $sql))) : $this;
    }
}
