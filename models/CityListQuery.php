<?php

namespace yii2portal\news\models;

/**
 * This is the ActiveQuery class for [[CityList]].
 *
 * @see CityList
 */
class CityListQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return CityList[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return CityList|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
