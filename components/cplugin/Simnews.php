<?php

namespace yii2portal\news\components\cplugin;

use yii2portal\cplugin\models\Cplugin;
use yii2portal\news\models\News;


class Simnews extends Cplugin{

    public function pluginChk($params) {
        return array(
            'status' => true,
            'params' => $params
        );
    }

    public function pluginConfig($params) {

        return array(
            'html' => $this->render('config', array('params' => $params)),
            'config' => true,
            'width' => $params['width'] ? $params['width'] : 276,
            'height' => $params['height'] ? $params['height'] : 300,
            'resizable' => true,
            'styles' => true,
            'style' => $params['style'] ? $params['style'] : 'float:right'
        );
    }

    public function pluginRender($params) {
        $return = '';
        if(isset($params['ids']) && !empty($params['ids'])){
            $ids = explode(',', $params['ids']);


            $news = News::getDb()->cache(function ($db) use($ids) {
                return News::find()
                    ->with('imageCol')
                    ->where(['id'=>$ids])
                    ->andPublished()
                    ->byDatepublic()
                    ->orderBy(['datepublic' => SORT_DESC])
                    ->all();
            });
            if (!empty($news)) {
                $params['width'] = $this->getSize($params['width']);
                $return = $this->render('simnews', array('news' => $news, 'params' => $params));
            }
        }
        return $return;
    }
}