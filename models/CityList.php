<?php

namespace yii2portal\news\models;

use Yii;

/**
 * This is the model class for table "city_list".
 *
 * @property integer $id
 * @property string $title
 * @property integer $def
 * @property integer $ord
 */
class CityList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'city_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['def', 'ord'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'def' => 'Def',
            'ord' => 'Ord',
        ];
    }

    /**
     * @inheritdoc
     * @return CityListQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CityListQuery(get_called_class());
    }
}
