<?php

namespace yii2portal\news\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii2portal\media\models\Media;
use yii2portal\structure\models\CoreStructure;
use yii2portal\video\models\VideoEls;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property integer $pid
 * @property integer $c_id
 * @property integer $m_id
 * @property integer $u_id
 * @property string $datecreate
 * @property integer $datepublic
 * @property integer $is_main
 * @property integer $is_day
 * @property integer $is_hour
 * @property string $title
 * @property string $description
 * @property string $lenta_title
 * @property string $lenta_description
 * @property string $int_title
 * @property string $int_description
 * @property string $content
 * @property string $persons
 * @property string $source
 * @property string $source_url
 * @property integer $status
 * @property integer $is_del
 * @property string $tags
 * @property string $olang
 * @property string $color
 * @property integer $is_bold
 * @property integer $partner
 * @property string $day_title
 * @property string $hour_title
 * @property string $hour_description
 * @property string $author
 * @property integer $is_afisha
 * @property integer $in_mailru
 * @property integer $in_mailru_rss
 * @property integer $in_yandex_rss
 * @property string $mailru_title
 * @property string $author_id
 * @property integer $cviews
 * @property string $foto_link
 * @property string $person_link
 * @property integer $actual
 * @property string $subscribers
 * @property integer $choose
 * @property integer $datelastedit
 * @property integer $type_id
 * @property integer $plan_id
 * @property integer $datelasteuserdit
 * @property string $comments
 * @property integer $is_colored
 * @property integer $stream
 * @property string $fotorep_title
 * @property integer $city
 * @property integer $agency
 * @property string $uplink
 * @property integer $left_datepublic
 * @property integer $top_datepublic
 * @property integer $image
 * @property integer $image_col
 * @property integer $image_lenta
 * @property integer $image_main
 * @property integer $show_gal
 * @property integer $last_edit
 * @property integer $press_time
 * @property string $branding
 * @property integer $is_main_cat
 * @property integer $is_photo
 * @property integer $is_video
 * @property integer $is_info
 * @property string $storify
 * @property string $tilda
 * @property string $theme_id
 * @property CoreStructure $category
 * @property CoreStructure $parent
 *
 * @property Media $imageLenta
 * @property Media $imageMain
 * @property Media $imageCol
 */
class News extends \yii\db\ActiveRecord
{

    private $_allAuthors = null;
    private $_galleryItems = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pid', 'c_id', 'm_id', 'u_id', 'datecreate', 'datepublic', 'is_main', 'is_day', 'is_hour', 'status', 'is_del', 'is_bold', 'partner', 'is_afisha', 'in_mailru', 'in_mailru_rss', 'in_yandex_rss', 'cviews', 'actual', 'choose', 'datelastedit', 'type_id', 'plan_id', 'datelasteuserdit', 'is_colored', 'stream', 'city', 'agency', 'left_datepublic', 'top_datepublic', 'image', 'image_col', 'image_lenta', 'image_main', 'show_gal', 'last_edit', 'press_time', 'is_main_cat', 'is_photo', 'is_video', 'is_info', 'theme_id'], 'integer'],
            [['m_id', 'u_id', 'title', 'description', 'lenta_title', 'lenta_description', 'int_title', 'int_description', 'content', 'is_colored'], 'required'],
            [['description', 'lenta_description', 'int_description', 'content', 'comments'], 'string'],
            [['title', 'lenta_title', 'int_title', 'persons', 'source', 'source_url', 'tags', 'day_title', 'hour_title', 'hour_description', 'author', 'mailru_title', 'author_id', 'foto_link', 'person_link', 'subscribers', 'fotorep_title', 'uplink', 'branding', 'storify', 'tilda'], 'string', 'max' => 255],
            [['olang'], 'string', 'max' => 5],
            [['color'], 'string', 'max' => 6],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pid' => 'Pid',
            'c_id' => 'C ID',
            'm_id' => 'M ID',
            'u_id' => 'U ID',
            'datecreate' => 'Datecreate',
            'datepublic' => 'Datepublic',
            'is_main' => 'Is Main',
            'is_day' => 'Is Day',
            'is_hour' => 'Is Hour',
            'title' => 'Title',
            'description' => 'Description',
            'lenta_title' => 'Lenta Title',
            'lenta_description' => 'Lenta Description',
            'int_title' => 'Int Title',
            'int_description' => 'Int Description',
            'content' => 'Content',
            'persons' => 'Persons',
            'source' => 'Source',
            'source_url' => 'Source Url',
            'status' => 'Status',
            'is_del' => 'Is Del',
            'tags' => 'Tags',
            'olang' => 'Olang',
            'color' => 'Color',
            'is_bold' => 'Is Bold',
            'partner' => 'Partner',
            'day_title' => 'Day Title',
            'hour_title' => 'Hour Title',
            'hour_description' => 'Hour Description',
            'author' => 'Author',
            'is_afisha' => 'Is Afisha',
            'in_mailru' => 'In Mailru',
            'in_mailru_rss' => 'In Mailru Rss',
            'in_yandex_rss' => 'In Yandex Rss',
            'mailru_title' => 'Mailru Title',
            'author_id' => 'Author ID',
            'cviews' => 'Cviews',
            'foto_link' => 'Foto Link',
            'person_link' => 'Person Link',
            'actual' => 'Actual',
            'subscribers' => 'Subscribers',
            'choose' => 'Choose',
            'datelastedit' => 'Datelastedit',
            'type_id' => 'Type ID',
            'plan_id' => 'Plan ID',
            'datelasteuserdit' => 'Datelasteuserdit',
            'comments' => 'Comments',
            'is_colored' => 'Is Colored',
            'stream' => 'Stream',
            'fotorep_title' => 'Fotorep Title',
            'city' => 'City',
            'agency' => 'Agency',
            'uplink' => 'Uplink',
            'left_datepublic' => 'Left Datepublic',
            'top_datepublic' => 'Top Datepublic',
            'image' => 'Image',
            'image_col' => 'Image Col',
            'image_lenta' => 'Image Lenta',
            'image_main' => 'Image Main',
            'show_gal' => 'Show Gal',
            'last_edit' => 'Last Edit',
            'press_time' => 'Press Time',
            'branding' => 'Branding',
            'is_main_cat' => 'Is Main Cat',
            'is_photo' => 'Is Photo',
            'is_video' => 'Is Video',
            'is_info' => 'Is Info',
            'storify' => 'Storify',
            'tilda' => 'Tilda',
            'theme_id' => 'Theme',
        ];
    }


    public function getCategory()
    {
        return $this->hasOne(CoreStructure::className(), ['id' => 'pid']);
    }

    public function getUrlPath()
    {
        $structure = Yii::$app->modules['structure'];
        return $structure->getUrl($this->pid) . "{$this->id}_" . $structure->makeUrl(html_entity_decode($this->title)) . '/';
    }

    public function getShortUrlPath()
    {
        $structure = Yii::$app->modules['structure'];
        return $structure->getUrl($this->pid) . "{$this->id}/";
    }


    public function dateFormat($format, $dateField = 'datepublic')
    {
        $formatter = Yii::$app->formatter;
        return $formatter->asDate($this->$dateField, $format);
    }

    public function getParent()
    {
        $parent = Yii::$app->getModule('structure')->getPage($this->pid);
        if (!$parent) {
            $parent = $this->category;
        }

        return $parent;
    }

    public function dateTimeFormat($format, $dateField = 'datepublic')
    {
        $formatter = Yii::$app->formatter;
        return $formatter->asDatetime($this->$dateField, $format);
    }


    public function getHasPhoto()
    {
        return $this->is_photo;
    }

    public function getHasVideo()
    {
        return $this->is_video;
    }

    public function getHasInfo()
    {
        return $this->is_info;
    }


    public function getGalleryItems()
    {
        if ($this->_galleryItems === null) {
            $this->_galleryItems = Media::find()->where([
                'groupid' => "news_{$this->id}"
            ])->all();
        }
        return $this->_galleryItems;
    }

    public function getImageCol()
    {
        return $this->hasOne(Media::className(), [
            'id' => 'image_col'
        ]);
    }

    public function getBaseImage()
    {
        return $this->hasOne(Media::className(), [
            'id' => 'image'
        ]);
    }

    public function getImageLenta()
    {
        return $this->hasOne(Media::className(), [
            'id' => 'image_lenta'
        ]);
    }

    public function getImageMain()
    {
        return $this->hasOne(Media::className(), [
            'id' => 'image_main'
        ]);
    }

    /**
     * @return mixed|Media
     */
    public function getAnyImage(){
        $image = $this->baseImage;
        if(!$image){
            $image = $this->imageLenta;
        }

        if(!$image){
            $image =$this->imageMain;
        }

        if(!$image){
            $image =$this->imageCol;
        }

        return $image;
    }

    public function getNewsAgency()
    {
        return $this->hasOne(Agency::className(), [
            'id' => 'agency'
        ]);
    }

    public function getNewsCity()
    {
        return $this->hasOne(CityList::className(), [
            'id' => 'city'
        ]);
    }

    public function getTagIds()
    {

        return empty($this->tags) ? [] : explode(',', $this->tags);
    }

    public function getNewsTags(){
        $return = [];
        if(!empty($this->tagIds)){
            $return = Tags::find()->where(['id'=>$this->tagIds])->all();
        }
        return $return;
    }

    public function getAllAuthors()
    {
        if ($this->_allAuthors === null) {
            $this->_allAuthors = [];
            $authors = Authors::find()->where([
                'id' => explode(',', $this->author_id)
            ])->all();

            if (!empty($authors)) {
                $this->_allAuthors = ArrayHelper::getColumn($authors, 'name');
            }

            if (!empty($this->author)) {
                $this->_allAuthors[] = $this->author;
            }
        }

        return $this->_allAuthors;
    }

    public function getVideo()
    {
        $videos = $this->videos;
        return !empty($videos) ? array_shift($videos) : null;
    }

    public function getVideos()
    {
        return $this->hasMany(VideoEls::className(), ['pid' => 'id']);
    }

    public function getNewsDescription()
    {
        $return = $this->description;
        if (mb_strlen($this->description) <= 10) {
            if (preg_match('#(<p[^>]*>.*?</p>)#uim', $this->content, $out)) {
                $return = strip_tags($out[0]);
            }
        }

        return $return;
    }

    /**
     * @inheritdoc
     * @return NewsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NewsQuery(get_called_class());
    }
}
