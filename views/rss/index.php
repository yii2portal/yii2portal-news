<?php

use \yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $page \yii2portal\structure\models\CoreStructure */
/* @var $news \yii2portal\news\models\News[] */


?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>';?>
<rss version="2.0">
    <channel>
        <title><?php echo $page->title; ?></title>
        <description><?php echo $page->title; ?></description>
        <language>ru</language>
        <docs>http://blogs.law.harvard.edu/tech/rss</docs>
        <link><?php echo Url::to($page->title, true); ?></link>
        <category><?php echo $page->title; ?></category>
        <lastBuildDate><?php echo date("r")?></lastBuildDate>
        <ttl>10</ttl>
        <?php foreach ($news as $new): ?>
            <item>
                <title><?php echo $new->title; ?></title>
                <link><?php echo Url::to($new->urlPath, true); ?></link>
                <description><?php echo $new->newsDescription; ?></description>

                <category><?php echo $new->parent->title; ?></category>
                <?php if ($new->imageLenta): ?>
                    <enclosure url='<?php echo Url::to($new->imageLenta->srcUrl, true) ?>' type='<?php echo $new->imageLenta->content_type?>' length='<?php echo $new->imageLenta->file_size;?>'/>
                <?php endif; ?>

                <pubDate><?php echo date('r', $new->datepublic); ?></pubDate>
                <guid><?php echo Url::to($new->urlPath, true); ?></guid>
            </item>
        <?php endforeach; ?>
    </channel>
</rss>
