<?php

namespace yii2portal\news\controllers;

use Yii;
use yii\web\Response;
use yii2portal\core\controllers\Controller;
use yii2portal\news\models\News;

/**
 * Site controller
 */
class RssController extends Controller
{

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\PageCache',
                'only' => ['index'],
                'dependency' => [
                    'class' => 'yii2portal\news\components\CacheDependency',
                ],
                'variations' => [
                    \Yii::$app->request->get('structure_id'),
                ],
                'duration' => 60*60*5,
            ],
        ];
    }


    public function actionIndex($structure_id)
    {
        Yii::$app->response->format = Response::FORMAT_RAW;
        $headers = Yii::$app->response->headers;
        $headers->add('Content-Type', 'text/xml');

        $news = News::find()
            ->with('imageLenta', 'newsAgency', 'newsAgency.imageLogo')
            ->andWhere([
                'pid' => $structure_id
            ])
            ->andPublished()
            ->orderBy(['datepublic' => SORT_DESC])
            ->byDatepublic()
            ->limit(50)
            ->all();


        return $this->renderPartial('index', [
            'page' => Yii::$app->getModule('structure')->getPage($structure_id),
            'news' => $news
        ]);
    }


}
