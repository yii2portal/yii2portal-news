<?php


return [
    'components'=>[
        'block'=>[
            'class'=>'yii2portal\news\components\block\Block'
        ],
        'meta'=>[
            'class'=>'yii2portal\news\components\Meta'
        ]
    ]
];