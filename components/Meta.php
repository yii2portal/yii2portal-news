<?php


namespace yii2portal\news\components;

use yii2portal\core\components\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii2portal\media\models\Media;
use yii2portal\news\models\News;

class Meta extends Widget
{

    /**
     * @param $new News
     * @param $defaultImg Media
     */
    public function register($new, $defaultImg = null)
    {

        $image = $new->getAnyImage();
        $imagePath = null;
        if(!$image && $defaultImg && $defaultImg instanceof Media){
            $image = $defaultImg;
        }
        if($image){
            $imagePath = Url::to($image->srcUrl, true);
        }


        $title = html_entity_decode(strip_tags($new->title));
        $description = html_entity_decode(strip_tags($new->newsDescription));

        $this->view->registerMetaTag([
            'content' => $description,
            'name' => 'description'
        ], 'description');
        $this->view->registerMetaTag([
            'content' => $title,
            'name' => 'title'
        ], 'title');
        if(!empty($new->allAuthors)) {
            $this->view->registerMetaTag([
                'content' => trim(implode(', ', $new->allAuthors)),
                'name' => 'author'
            ], 'author');
/*            $this->view->registerMetaTag([
                'content' => trim(implode(', ', $new->allAuthors)),
                'property' => 'article:author'
            ], 'article:author');*/
        }
        $this->view->registerMetaTag([
            'content' => $description,
            'property' => 'og:description'
        ], 'og:description');
        $this->view->registerMetaTag([
            'content' => $title,
            'property' => 'og:title'
        ], 'og:title');

        if($imagePath) {
            $this->view->registerMetaTag([
                'content' => $imagePath,
                'property' => 'og:image'
            ], 'og:image');
            $this->view->registerMetaTag([
                'content' => $imagePath,
                'name' => 'image_src'
            ], 'image_src');
            $this->view->registerMetaTag([
                'content' => $imagePath,
                'name' => 'twitter:image:src'
            ], 'twitter:image:src');
        }
        $this->view->registerMetaTag([
            'content' => Url::to($new->urlPath, true),
            'property' => 'og:url'
        ], 'og:url');
        $this->view->registerMetaTag([
            'content' => 'article',
            'property' => 'og:type'
        ], 'og:type');
        $this->view->registerMetaTag([
            'content' => '24.kg',
            'property' => 'og:site_name'
        ], 'og:site_name');
        $this->view->registerMetaTag([
            'content' => 'https://www.facebook.com/www.24.kg',
            'property' => 'article:publisher'
        ], 'article:publisher');
        $this->view->registerMetaTag([
            'content' => date('c', $new->datepublic),
            'property' => 'article:published_time'
        ], 'article:published_time');
        $this->view->registerMetaTag([
            'content' => date('c', $new->datelasteuserdit),
            'property' => 'article:modified_time'
        ], 'article:modified_time');
        $this->view->registerMetaTag([
            'content' => $new->parent->title,
            'property' => 'article:section'
        ], 'article:section');
        $this->view->registerMetaTag([
            'content' => $description,
            'name' => 'twitter:description'
        ], 'twitter:description');
        $this->view->registerMetaTag([
            'content' => 'summary_large_image',
            'name' => 'twitter:card'
        ], 'twitter:card');
        $this->view->registerMetaTag([
            'content' => '@_24_kg',
            'name' => 'twitter:site'
        ], 'twitter:site');
        $this->view->registerMetaTag([
            'content' => '249014600',
            'name' => 'twitter:site:id'
        ], 'twitter:site:id');
        $this->view->registerMetaTag([
            'content' => $title,
            'name' => 'twitter:title'
        ], 'twitter:title');
    }

}