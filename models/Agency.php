<?php

namespace yii2portal\news\models;

use Yii;
use yii2portal\media\models\Media;

/**
 * This is the model class for table "agency".
 *
 * @property integer $id
 * @property string $title
 * @property integer $logo
 * @property string $url
 * @property integer $ord
 * @property integer $def
 */
class Agency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['logo', 'ord', 'def'], 'integer'],
            [['title', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'logo' => 'Logo',
            'url' => 'Url',
            'ord' => 'Ord',
            'def' => 'Def',
        ];
    }

    public function getNews(){
        return $this->hasMany(News::className(), [
            'agency'=>'id'
        ]);
    }

    public function getImageLogo(){
        return $this->hasOne(Media::className(), [
            'id'=>'logo'
        ]);
    }

    /**
     * @inheritdoc
     * @return AgencyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AgencyQuery(get_called_class());
    }
}
