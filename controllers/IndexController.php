<?php

namespace yii2portal\news\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use yii2portal\core\controllers\Controller;
use yii2portal\news\models\News;

/**
 * Site controller
 */
class IndexController extends Controller
{

    public function actionIndex($structure_id)
    {
        
        $query = News::find()->where([
            'pid' => $structure_id,
            'status' => 1
        ])->byDatepublic();

        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
//                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'datepublic' => SORT_DESC
                ]
            ],
        ]);

        return $this->render('index', [
            'page' => Yii::$app->modules['structure']->getPage($structure_id),
            'provider' => $provider
        ]);
    }

    public function actionView($structure_id, $item_id)
    {


        $new = News::findOne($item_id);
        if(!$new){
            throw new NotFoundHttpException();
        }

        return $this->render('view', [
            'category' => Yii::$app->modules['structure']->getPage($structure_id),
            'new' => $new
        ]);
    }


}
